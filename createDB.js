var mongoose = require('libs/mongoos');
var async = require('async');
mongoose.set('debug', true);
async.series([
    open,
    dropDataBase,
    requireModels,
    createUsers
], function(err, resalts){
    console.log(arguments);
    mongoose.disconnect();
    process.exit(err ? 255 : 0)
})


function open(callback){
   mongoose.connection.on('open', callback);
}

function dropDataBase(callback){
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback){
    require('models/user');
    async.each(Object.keys(mongoose.models), function(modelName, callback){
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
}

function createUsers(callback){

    var users = [
        {userId:'f:werdtgyuhi', userName:"test"},
        {userId:'f:srdftyu', userName:"test"},
        {userId:'f:fjhjdsdf', userName:"test"}
    ]
     async.each(users, function(userData, callback){
         var user = new mongoose.models.User(userData);
         user.save(callback);
     }, callback);

}

