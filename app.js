
/**
 * Module dependencies.
 */

var express = require('express');
//var routes = require('./routes');
//var user = require('./routes/user');
var http = require('http');
var path = require('path');
var User = require('models/user').User;
var mongoose = require('libs/mongoos');
var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(require('connect').bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//app.get('/', routes.index);
//app.get('/users', user.list);

app.post('/api/login', function(req, res){
    var user = new User({
        userId: req.body.userId,
        userName: req.body.userName,
        userSername:req.body.userSername,
        url: req.body.url,
        score: req.body.score
    });

    user.save(function (err) {
        if (!err) {
           // log.info("article created");
            return res.send({ status: 'OK', user:user });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            //log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

app.post('/api/update_score', function(req, res){

    var query = { userId: req.body.userId};
    User.findOneAndUpdate(query, { score:  req.body.score}, function(err, user){
        if(!user) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send(user);
        } else {
            res.statusCode = 500;
            // log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });

});

app.post('/api/list', function(req, res) {
    var users = new Array();
    function logArrayElements(element, index, array) {

        if (index != array.length){
            User.find({userId: element}, function (err, user) {
                //var jsObject = JSON.parse(req.body);

                users.push(user);
                console.log('request =' + JSON.stringify(req.body.users))
                if(!users) {
                    res.statusCode = 404;
                    return res.send({ error: 'Not found' });
                }
                if (!err) {
                    if (index >= array.length - 1)return res.send(users);
                } else {
                    res.statusCode = 500;
                    // log.error('Internal error(%d): %s',res.statusCode,err.message);
                    return res.send({ error: 'Server error' });
                }
            });

        }

    }

    var par = JSON.parse(req.body.users);
    console.log(par);
    par.forEach(logArrayElements);


});

app.post('/', function(request, response){
    console.log(request.body);      // your JSON
    response.send(request.body);    // echo the result back
});



http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
