var crypto = require("crypto");

var mongoose = require("libs/mongoos"),
    Schema = mongoose.Schema


var schema = new Schema({
    userId:{
        type:String,
        unique:true,
        required:true
    },
    userName:{
        type:String
    },
    userSername:{
        type:String
    },
    url:{
        type:String
    },
    score:{
        type:String
    },
    created:{
        type:Date,
        default:Date.now
    }
});


exports.User = mongoose.model('User', schema);